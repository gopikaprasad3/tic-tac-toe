print('TIC TAC TOE')
dict={}
def display_board(board):
    print(board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('-----------')
    print(board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('------------')
    print(board[7] + ' | ' + board[8] + '  | ' + board[9])

def getBoardCopy(board):
    dupeBoard = []
    for i in board:
        dupeBoard.append(i)
    return dupeBoard

def player_input():
    marker = ''
    while not (marker == 'X' or marker == 'O'):
        print('Do you want to be X or O?')
        marker = input().upper()
    if marker == 'X':
        return ['X', 'O']
    else:
        return ['O', 'X']


def chooseRandomMoveFromList(board, movesList):
    possibleMoves = []
    for i in movesList:
        if space_check(board, i):
            possibleMoves.append(i)
            if len(possibleMoves) != 0:
                return random.choice(possibleMoves)
            else:
                return None


def getComputerMove(board, computerLetter):
    if computerLetter == 'X':
        playerLetter = 'O'
    else:
        playerLetter = 'X'

    for i in range(1, 10):
       copy = getBoardCopy(board)
       if space_check(copy,i):
           place_marker(copy, computerLetter, i)
           if win_check(copy, computerLetter):
            return i
    position = chooseRandomMoveFromList(board, [1, 3, 7, 9])
    if position != None:
        return position

    if space_check(board, 5):
        return 5
    return chooseRandomMoveFromList(board, [2, 4, 6, 8])


def place_marker(board, marker, position):
    board[position] = marker

def win_check(board,mark):
    return((board[7] == board[8] == board[9] == mark) or
           (board[4] == board[5] == board[6] == mark) or
           (board[1] == board[2] == board[3] == mark) or
           (board[7] == board[4] == board[1] == mark) or
           (board[8] == board[5] == board[2] == mark) or
           (board[9] == board[6] == board[3] == mark) or
           (board[7] == board[5] == board[3] == mark) or
           (board[9] == board[5] == board[1] == mark))

def space_check(board, position):
    return board[position] == ''

def full_board_check(board):
    for i in range (1,10):
        if space_check(board,i):
            return False
    return True

def player_choice(board):
    position = 0
    while position not in [1, 2, 3, 4, 5, 6, 7, 8, 9] or not space_check(board,position):
        position = int(input('choose a position :(1-9)'))
    return position


def replay():
    print('Do you want to play again? (yes or no)')
    return input().lower().startswith('y')

import random
def choose_first():
    if random.randint(0, 1) == 0:
        return 'computer'
    else:
        return 'player'

while True:
    for i in range(1,11):
        list =[]
        dict={}
        the_board = [''] * 10
        playerLetter, computerLetter = player_input()
        turn = choose_first()
        # print(turn + ' will go first')
        play_game = input('ready to play? y or n ?')
        if play_game == 'y':
            game_on = True
        else:
            game_on = False
        while game_on:

            if turn == 'player1':
                display_board(the_board)
                position = player_choice(the_board)
                place_marker(the_board, playerLetter, position)
                if win_check(the_board, playerLetter):
                    display_board(the_board)
                    print('YOU WON')
                    dict = {i:[the_board[1],the_board[2],the_board[3],the_board[4],the_board[5],the_board[6],the_board[7],the_board[8],the_board[9]]}
                    print(dict)
                    game_on = False
                else:
                    if full_board_check(the_board):
                        display_board(the_board)
                        print('TIE GAME')
                        list = [[the_board[1],the_board[2],the_board[3],the_board[4],the_board[5],the_board[6],the_board[7],the_board[8],the_board[9]]]
                        print(dict)
                        game_on = False
                    else:
                        turn = 'player2'
            else:
                # display_board(the_board)
                position = getComputerMove(the_board, computerLetter)
                place_marker(the_board, computerLetter, position)
                if win_check(the_board, computerLetter):
                    display_board(the_board)
                    print('COMPUTER WON')
                    dict = {i:[the_board[1],the_board[2],the_board[3],the_board[4],the_board[5],the_board[6],the_board[7],the_board[8],the_board[9]]}
                    print(dict)
                    game_on = False
                else:
                    if full_board_check(the_board):
                        display_board(the_board)
                        print('TIE GAME')
                        dict = {i:[the_board[1],the_board[2],the_board[3],the_board[4],the_board[5],the_board[6],the_board[7],the_board[8],the_board[9]]}
                        print(dict)
                        break

                    else:
                       turn = 'player1'
round = int(input('enter the round')
out = dict.items
print(out)

